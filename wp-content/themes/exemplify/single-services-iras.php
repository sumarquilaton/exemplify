<?php
/**
 * Single IRAS
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
    ?>
    <div class="section-banner section-gradient banner-subpage">
        <div class="banner-img bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/ira-banner.jpg');"></div>
        <div class="caption transform-50 text-center animatedParent animateOnce">
            <h1 class="animated fadeInUpShort"><?php the_title(); ?></h1>
        </div>
    </div>
    <div class="section-content section-content-subpage">
        <div class="container">
            <div class="single-content">
                <div class="animatedParent animateOnce">
                    <div class="single-iras-content afterclear">
                        <div style="text-align: center; max-width: 900px; margin: 0 auto;">
                            <?php the_field('description'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-gradient folds bg-inline bg-fixed" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/ira-retire.jpg');">
        <div class="container">
            <div class="white-caption ira-type-caption afterclear animatedParent animateOnce">
                <div class="section-caption text-center">
                    <h2 class="animated fadeInUpShort"><?php the_field('blue_section_title'); ?></h2>
                </div>
                <div class="news-list afterclear animated fadeInUp slow delay-250">
                    <div class="gap-80"></div>
                    <?php while (have_rows('ira_types')): the_row(); ?>
                    <div class="col-md-4">
                        <a href="<?php echo site_url(); ?><?php the_sub_field('type_link'); ?>">
                            <div class="news-list-content ira-content">
                                <h3><?php the_sub_field('type_title'); ?></h3>
                                <div class="gap-20"></div>
                                <?php the_sub_field('type_content'); ?>
                                <div class="gap-30"></div>
                                <btn class="btn-common btn-white">Read more</btn>
                            </div>
                        </a>
                    </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content">
        <div class="container">
            <div class="single-content">
                <div class="animatedParent animateOnce">
                    <div class="col-md-6">
                        <div class="gap-80"></div>
                        <div class="section-caption animated fadeInUp slow">
                            <?php the_field('left_content'); ?>
                        </div>
                    </div>
                    <div class="col-md-6 animated fadeInRight slow">
                        <div class="single-service-img-holder">
                            <div class="single-service-img bg-inline" style="background-image: url('<?php the_field('right_image'); ?>');"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-gradient folds bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/ira-bene.png');">
        <div class="container animatedParent animateOnce">
            <div class="white-caption animated fadeInUp slow afterclear text-center">
                <?php the_field('bottom_blue_content'); ?>
            </div>
        </div>
    </div>

    <?php if(trim(get_field('bottom_content',$post->ID))): ?>
        <div class="section-content footer-section animatedParent animateOnce">
            <div class="container">
                <div class="col-md-5 animated fadeInDownShort">
                    <div class="section-about">
                        <div class="about-img bg-inline" style="background-image: url('<?php the_field('bottom_image'); ?>'); "></div>
                    </div>
                </div>
                <div class="col-md-7 animated fadeInRight slow">
                    <div class="gap-50"></div>
                    <div class="section-caption pl-4">
                        <?php the_field('bottom_content'); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endif;?>
    <?php
endwhile; else :
endif;
get_footer(); ?>