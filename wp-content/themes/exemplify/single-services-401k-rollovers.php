<?php
/**
 * Single 401K's
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="section-banner section-gradient banner-subpage">
        <div class="banner-img bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/401k-banner.png');"></div>
        <div class="caption transform-50 text-center animatedParent animateOnce">
            <h1 class="animated fadeInUpShort">401K's</h1>
        </div>
    </div>
    <div class="section-content section-about section-content-subpage animatedParent animateOnce">
        <div class="container">
            <div class="section-caption text-center">
                <?php the_field('401k_description'); ?>
            </div>
        </div>
    </div>
    <div class="section-content section-gradient folds bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/401k-1.png');">
        <div class="container animatedParent animateOnce">
            <div class="white-caption afterclear text-center animated fadeInUp slow">
                <?php the_field('401k_blue_content'); ?>
            </div>
        </div>
    </div>
    <div class="section-content footer-section animatedParent animateOnce">
        <div class="container">
            <div class="col-md-7 animated fadeInRight slow">
                <div class="section-caption">
                    <?php the_field('401k_left_content'); ?>
                </div>
            </div>
            <div class="col-md-5 animated fadeInDownShort">
                <div class="section-about">
                    <div class="about-img about-mtop bg-inline" style="background-image: url('<?php the_field('401k_right_image'); ?>'); "></div>
                </div>
            </div>
        </div>
        <div class="gap-80"></div>
    </div>
    <div class="section-content section-content-subpage section-services-list animatedParent animateOnce">
        <div class="container">
            <div class="section-caption animated fadeInUp slow">
                <?php the_field('401k_feature_content'); ?>
            </div>
            <div class="gap-80"></div>
            <div class="key-holder animated fadeInUp slow delay-500">
                <?php while (have_rows('401k_features')): the_row(); ?>
                <div class="col-md-6 p-0">
                    <div class="white-bg">
                        <h3><?php the_sub_field('feature_title'); ?></h3>
                        <div class="gap-20"></div>
                        <p><?php the_sub_field('feature_content'); ?></p>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
        </div>
        <div class="gap-80"></div>
    </div>
    <div class="section-content">
        <div class="container">
            <div class="single-content">
                <div class="animatedParent animateOnce">
                    <div class="col-md-6 animated fadeInLeft slow">
                        <div class="single-service-img-holder">
                            <div class="single-service-img bg-inline" style="background-image: url('<?php the_field('401k_bottom_image'); ?>');"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="gap-80"></div>
                        <div class="section-caption pl-4 animated fadeInUp slow">
                            <?php the_field('401k_bottom_content'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-gradient folds bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/bottom-bg.jpg');">
        <div class="container animatedParent animateOnce">
            <div class="white-caption afterclear text-center">
                <h2 class="animated fadeInUpShort">Request More Information</h2>
            </div>
            <div class="form-holder text-center afterclear animated fadeInUpShort delay-250">
                <div class="gap-50"></div>
                <?php echo do_shortcode('[contact-form-7 id="105" title="Information"]'); ?>
            </div>
        </div>
    </div>
    <?php
endwhile; else :
endif;
get_footer(); ?>