<?php
/**
 * Template Name: Contact Page
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
<div class="section-banner section-gradient banner-subpage">
    <div class="banner-img bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banner-contact.jpg');"></div>
    <div class="caption transform-50 text-center animatedParent animateOnce">
        <h1 class="animated fadeInUpShort">Contact Us</h1>
    </div>
</div>
<div class="section-content section-content-subpage animatedParent animateOnce">
    <div class="container">
        <div class="col-md-1"></div>
        <div class="col-md-5 animated fadeInLeft">
            <div class="contact-details">
                <h1>Contacts</h1>
                <div class="gap-20"></div>
                <div class="contact-desc">
                    <div class="icon-social">
                        <i class="fa fa-phone"></i>
                    </div>
                    <div class="social-desc">
                        <?php the_field('phone_content', 'option'); ?>
                    </div>
                </div>
                <div class="contact-desc">
                    <div class="icon-social">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="social-desc">
                        <?php the_field('web_content', 'option'); ?>
                    </div>
                </div>
                <div class="gap-15"></div>
            </div>
            <div class="contact-details">
                <h1>Address</h1>
                <div class="gap-20"></div>
                <div class="contact-desc">
                    <div class="icon-social">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="social-desc">
                        <?php the_field('address_content', 'option'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 animated fadeInRight">
            <div class="form-contact">
                <?php echo do_shortcode('[contact-form-7 id="6" title="Contact form 1"]'); ?>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
<div class="section-map animatedParent animateOnce">
    <div class="container animated fadeInUp delay-250">
        <div id="map">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3352.864830316724!2d-96.7927690844435!3d32.8223462809584!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x864e9f20cf4b4c1f%3A0x1a61f0ef550de1ab!2s4514+Travis+St+%23300%2C+Dallas%2C+TX+75205%2C+USA!5e0!3m2!1sen!2sph!4v1491382458861" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
</div>
    <div class="section-content section-gradient folds bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/consult-bg.png');">
        <div class="container">
            <div class="white-caption afterclear animatedParent animateOnce">
                <div class="col-md-8 animated fadeInLeft">
                    <?php the_field('consultation_content',4); ?>
                </div>
                <div class="col-md-4 animated fadeInRight">
                    <a href="<?php echo site_url(); ?>/<?php the_field('consultation_link',4); ?>" class="btn-common btn-white">Free Consultation</a>
                </div>
            </div>
        </div>
    </div>
<?php
endwhile; else :
endif;
get_footer(); ?>