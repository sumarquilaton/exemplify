<?php
/**
 * Template Name: About Us
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
    ?>
    <div class="section-banner section-gradient banner-subpage">
        <div class="banner-img bg-inline" style="background-image: url('<?php echo $bg_img[0]; ?>');"></div>
        <div class="caption transform-50 text-center animatedParent animateOnce">
            <h1 class="animated fadeInUpShort">About Us</h1>
        </div>
    </div>
    <div class="section-content section-about section-content-subpage animatedParent animateOnce">
        <div class="container">
            <div class="section-caption text-center">
                <h2 class="animated fadeInUp slow">Company Overview</h2>
                <div class="gap-20"></div>
                <?php the_field('company_overview'); ?>
            </div>
        </div>
    </div>
    <div class="section-content section-gradient folds bg-inline bg-fixed" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/bottom-bg.jpg');">
        <div class="container animatedParent animateOnce">
            <div class="white-caption afterclear text-center animated fadeInUp slow delay-250">
                <?php the_field('about_blue_section_content'); ?>
            </div>
            <div class="gap-80"></div>
            <div class="different-section white-caption text-center animated fadeInUp slow delay-500">
                <ul>
                    <?php while (have_rows('different_list')): the_row(); ?>
                        <li>
                            <div class="diff-icon">
                                <img src="<?php the_sub_field('different_icon'); ?>" alt="">
                            </div>
                            <div class="gap-20"></div>
                            <div class="diff-title">
                                <h3><?php the_sub_field('different_title'); ?></h3>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="section-content footer-section animatedParent animateOnce">
        <div class="container">
            <div class="col-md-7 animated fadeInDownShort">
                <div class="section-caption">
                    <h2>Company History</h2>
                    <div class="gap-20"></div>
                    <div class="company-img bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/company.jpg'); "></div>
                </div>
            </div>
            <div class="col-md-5 animated fadeInRight slow">
                <div class="gap-95"></div>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <div class="section-caption">
                                <h3>Starts at Lorem Ipsum</h3>
                                <p>21st October 2016 </p>
                                <div class="gap-20"></div>
                                <p>Maecenas eu lectus vitae massa auctor posuere. Ut pretium placerat fermentum. Mauris tincidunt placerat nibh, a gravida turpis vulputate ut. Nulla tempus ultrices risus, sed congue leo dapibus non. Aenean congue eget eros eu ultricies. Pellentesque nec faucibus nisl, et dictum neque. Mauris erat felis, placerat et efficitur molestie, vestibulum vitae erat. Sed scelerisque commodo massa.</p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="section-caption">
                                <h3>Starts at Lorem Ipsum</h3>
                                <p>21st October 2016 </p>
                                <div class="gap-20"></div>
                                <p>Maecenas eu lectus vitae massa auctor posuere. Ut pretium placerat fermentum. Mauris tincidunt placerat nibh, a gravida turpis vulputate ut. Nulla tempus ultrices risus, sed congue leo dapibus non. Aenean congue eget eros eu ultricies. Pellentesque nec faucibus nisl, et dictum neque. Mauris erat felis, placerat et efficitur molestie, vestibulum vitae erat. Sed scelerisque commodo massa.</p>
                            </div>
                        </div>
                    </div>

                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content footer-section animatedParent animateOnce">
        <div class="container">
            <div class="section-caption">
                <h2 class="animated fadeInUpShort go">Paul A. Sartin</h2>
                <h4 class="animated fadeInUpShort go">Chief Executive Officer</h4>
                <div class="gap-30"></div>
            </div>
            <div class="section-caption">
                <div class="gap-20"></div>
                <div class="col-md-6 p-0">
                    <div class="bio-desc mw-600">
                        <p>Paul A. Sartin is a Dallas native, and attorney who has long specialized in financial and retirement planning in his practice. Paul attended St. Mark’s School of Texas for high school locally.  Upon graduation, he attended The University of Texas at Austin (graduating with B.A. – Plan II Honors).  He furthered his education at Southern Methodist University – Dedman School of Law (J.D.) and following his parents, Peter A. T. Sartin and Linda D. Sartin, into the practice of law.</p>
                        <p>
                            Paul utilizes his experience as a former practicing corporate and probate, trusts and estates attorney, as well as his experience in the insurance and brokerage areas, to assist his clients within the financial planning and retirement fields.</p>
                    </div>
                </div>
                <div class="col-md-6 p-0">
                    <div class="bio-desc mw-600">
                        <p>In Exemplify Financial Services, LLC, Paul has specifically put together a team with experience in all aspects of the life settlement and viatical business, including policy procurement, contract negotiations, underwriting, life expectance analysis, the bidding process, pricing, and case submission. In addition, Paul has acquired a large network of business and legal professionals upon which he continues to build within the Dallas-Fort Worth metroplex and all across Texas in order to achieve Exemplify’s clients’ wealth management goals.</p>
                        <p>Paul is a current member of the State Bar of Texas, the Texas Association of Business Brokers, the Probate, Trusts and Estates section of the Dallas Bar Association, and Texas Exes Lifetime Member.
                        </p>
                    </div>
                </div>

            </div>
        </div>
        <div class="gap-80"></div>
    </div>
    <div class="section-content section-gradient folds bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/company-bg.jpg');">
        <div class="container animatedParent animateOnce">
            <div class="white-caption afterclear text-center animated fadeInUp slow">
                <h2>Company’s Values and Philosophy</h2>
                <div class="gap-20"></div>
                <?php the_field('values_content'); ?>
            </div>
        </div>
    </div>
    <div class="section-content footer-section animatedParent animateOnce">
        <div class="container">
            <div class="col-md-7 animated fadeInLeft slow">
                <div class="section-caption">
                    <?php the_field('about_left_content'); ?>
                </div>
            </div>
            <div class="col-md-5 animated fadeInDownShort">
                <div class="section-about">
                    <div class="about-img bg-inline" style="background-image: url('<?php the_field('about_right_image'); ?>'); "></div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-gradient folds bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/bottom-bg.jpg');">
        <div class="container animatedParent animateOnce">
            <div class="white-caption afterclear text-center">
                <h2 class="animated fadeInUpShort">Request More Information</h2>
            </div>
            <div class="form-holder text-center afterclear animated fadeInUpShort delay-250">
                <div class="gap-50"></div>
                <?php echo do_shortcode('[contact-form-7 id="105" title="Information"]'); ?>
            </div>
        </div>
    </div>
    <?php
endwhile; else :
endif;
get_footer(); ?>