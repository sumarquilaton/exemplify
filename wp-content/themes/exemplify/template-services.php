<?php
/**
 * Template Name: Services Page
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
<div class="section-banner section-gradient banner-subpage">
    <div class="banner-img bg-inline" style="background-image: url('<?php echo $bg_img[0]; ?>');"></div>
    <div class="caption transform-50 text-center animatedParent animateOnce">
        <h1 class="animated fadeInUpShort">Services</h1>
    </div>
</div>
<div class="section-content section-content-subpage section-sub animatedParent animateOnce">
    <div class="container">
        <div class="section-caption">
            <div class="sep"></div>
            <?php the_content(); ?>
        </div>
    </div>

    <div class="form-search copy-gap">
        <form role="search" method="get" class="search-form" action="<?php echo site_url(); ?>">
            <label>
                <input type="search" class="form-control" placeholder="Search" value="" name="s">
            </label>
            <input type="submit" class="btn-common btn-black" value="Search">
        </form>
    </div>

    <div class="section-services-grid animated fadeInUp delay-500 slow">
        <div class="container">
            <?php
            query_posts(array(
                    'post_type' => 'services',
                    'posts_per_page' => 18,
            ));
            if ( have_posts() ) : ?>
                <?php while(have_posts()) : the_post(); ?>

                <div class="col-md-6">
                    <a href="<?php the_permalink();?>">
                        <div class="service-holder">
                            <div class="service-img-holder">
                                <div class="service-img bg-inline" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));?>'); "></div>
                            </div>
                            <div class="service-caption">
                                <div class="service-title">
                                    <div class="service-title-l1">
                                        <h3><?php the_title(); ?></h3>
                                    </div>
                                    <div class="separator"></div>
                                </div>
                                <div class="service-desc">
                                    <p><?php the_field('services_content'); ?> <span>See More <i class="fa fa-long-arrow-right" aria-hidden="true"></i></span></p>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <?php endwhile; wp_reset_query(); ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="section-content section-gradient folds bg-inline" style="background-image: url('images/bottom-bg.jpg');">
    <div class="container animatedParent animateOnce">
        <div class="white-caption afterclear text-center">
            <h2 class="animated fadeInUpShort">Request More Information</h2>
        </div>
        <div class="form-holder text-center afterclear animated fadeInUpShort delay-250">
            <div class="gap-50"></div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Full Name">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Email Address">
            </div>
            <div class="form-group">
                <btn class="btn-common btn-white">Submit</btn>
            </div>
        </div>
    </div>
</div>
<?php
endwhile; else :
endif;
get_footer(); ?>