<?php
/**
 * Single Services
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
    ?>
    <div class="section-banner section-gradient banner-subpage">
        <div class="banner-img bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banner-subpage.jpg');"></div>
        <div class="caption transform-50 text-center animatedParent animateOnce">
            <h1 class="animated fadeInUpShort"><?php the_title(); ?></h1>
        </div>
    </div>
    <div class="section-content section-content-subpage">
        <div class="container">
            <div class="single-content">
                <div class="animatedParent animateOnce">
                    <div class="col-md-6">
                        <div class="section-caption animated fadeInUp slow">
                            <h3 class="mw-600"><?php the_field('sub_title'); ?></h3>
                            <div class="gap-30"></div>
                            <div class="pr-5">
                                <?php the_field('services_template_description'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 animated fadeInRight slow">
                        <div class="single-service-img-holder">
                            <div class="single-service-img bg-inline" style="background-image: url('<?php echo $bg_img[0]; ?>');"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-gradient folds bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/consult-bg.png');">
        <div class="container">
            <div class="white-caption afterclear animatedParent animateOnce">
                <div class="col-md-8 animated fadeInLeft">
                    <?php the_field('consultation_content',4); ?>
                </div>
                <div class="col-md-4 animated fadeInRight">
                    <a href="<?php echo site_url(); ?>/<?php the_field('consultation_link',4); ?>" class="btn-common btn-white">Free Consultation</a>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content footer-section animatedParent animateOnce">
        <div class="container">
            <div class="section-caption text-center">
                <h2>Other Services</h2>
            </div>
            <div class="services-list-grid afterclear animated fadeInUp slow delay-500">
                <?php
                $args = array(
                        'post_type' => 'services',
                        'posts_per_page' =>3,
                        'tax_query' => array(
                                array(
                                        'taxonomy' => 'servicescat',
                                        'field' => 'id',
                                        'terms' => 3
                                )
                        ));
                $query = new WP_Query( $args );
                ?>

                <?php
                // The Loop
                if ( $query->have_posts() ) :
                    while ( $query->have_posts() ) :
                        $query->the_post(); ?>
                        <div class="col-md-4">
                            <a href="<?php the_permalink(); ?>">
                                <div class="services-grid-holder section-gradient bg-inline" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));?>'); "></div>
                                <div class="services-grid-content white-caption">
                                    <h3><?php the_title(); ?></h3>
                                    <div class="gap-15"></div>
                                    <p><?php the_field('services_content'); ?></p>
                                </div>
                            </a>
                        </div>
                    <?php endwhile;
                endif;
                /* Restore original Post Data */
                wp_reset_postdata();
                ?>
            </div>
            <div class="read-more text-center">
                <div class="gap-80"></div>
                <a href="<?php echo site_url(); ?>/<?php the_field('services_link'); ?>" class="btn-common btn-blue">Read more</a>
            </div>
        </div>
    </div>
    <div class="section-content section-gradient folds bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/bottom-bg.jpg');">
        <div class="container animatedParent animateOnce">
            <div class="white-caption afterclear text-center">
                <h2 class="animated fadeInUpShort">Request More Information</h2>
            </div>
            <div class="form-holder text-center afterclear animated fadeInUpShort delay-250">
                <div class="gap-50"></div>
                <?php echo do_shortcode('[contact-form-7 id="105" title="Information"]'); ?>
            </div>
        </div>
    </div>
    <?php
endwhile; else :
endif;
get_footer(); ?>