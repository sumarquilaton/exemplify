<?php
/**
 * Single Life Settlements
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <div class="section-banner section-gradient banner-subpage">
        <div class="banner-img bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/life-banner.jpg');"></div>
        <div class="caption transform-50 text-center animatedParent animateOnce">
            <h1 class="animated fadeInUpShort go"><?php the_title(); ?></h1>
        </div>
    </div>
    <div class="section-content section-about section-content-subpage animatedParent animateOnce">
        <div class="container">
            <div class="section-caption text-center">
                <h2 class="mw-750 animated fadeInDownShort go">What Are Life Settlement and Viatical Investments?</h2>
                <div class="gap-20"></div>
                <p class="mw-750 animated fadeInUp slow delay-250 go">Exemplify Financial Services, LLC manages a group of Funds which acquire interests in life insurance policy investments via life settlements and viaticals. A “life settlement” or “viatical” is the transfer of the beneficial interest in a life insurance policy, by the insured person through a provider to a third party (in this case, the Fund). The policy owner transfers their policy at a discount to its face value, in return for an immediate cash settlement. The purchaser of the policy is then responsible for premiums payable on the policy and will be entitled to receive the full-face value from the insurance company upon maturity (the passing of the insured).</p>
            </div>
        </div>
    </div>
    <div class="section-content section-gradient folds bg-inline bg-fixed" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/bottom-bg.jpg');">
        <div class="container animatedParent animateOnce">
            <div class="mw-750 white-caption afterclear text-center animated fadeInUp slow delay-250">
                <h3>Life settlements and viaticals can play an important part in maximizing your retirement portfolio for:</h3>
            </div>
            <div class="gap-80"></div>
            <div class="different-section life-grid white-caption text-center animated fadeInUp slow delay-500">
                <ul>
                    <li>
                        <div class="diff-icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/l1.png" alt="">
                        </div>
                        <div class="gap-20"></div>
                        <div class="diff-title">
                            <h3>Financial Prosperity</h3>
                        </div>
                    </li>
                    <li>
                        <div class="diff-icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/l2.png" alt="">
                        </div>
                        <div class="gap-20"></div>
                        <div class="diff-title">
                            <h3>A Comfortable Retirement</h3>
                        </div>
                    </li>
                    <li>
                        <div class="diff-icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/l3.png" alt="">
                        </div>
                        <div class="gap-20"></div>
                        <div class="diff-title">
                            <h3>Estate <br>Planning</h3>
                        </div>
                    </li>
                    <li>
                        <div class="diff-icon">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/l4.png" alt="">
                        </div>
                        <div class="gap-20"></div>
                        <div class="diff-title">
                            <h3>Philanthropy</h3>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="section-content footer-section animatedParent animateOnce">
        <div class="container">
            <div class="col-md-5 animated fadeInDownShort">
                <div class="mw-500 section-caption">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/l5.png" alt="">
                    <div class="gap-20"></div>
                    <h3>Why are Life Settlements and Viaticals Unique Investments?</h3>
                </div>
            </div>
            <div class="col-md-7 animated fadeInRight slow">
                <div class="gap-90"></div>
                <div class="life-content">
                    <p>The returns from life insurance policy investments (through life settlements and viaticals) are generally not correlated with other investment markets for a variety of reasons. Once a policy has been purchased, the benefit payable is known. This means, that the performance of the life insurance policies will be based on the characteristics of the pool of the insured lives, and not on the performance of traditional markets, such as the stock market, the real property market or the bond market.   Yield is determined by time, not market forces, so it’s not a question of ‘if’ a return will be paid, but ‘when’ it will be paid.
                    </p>
                </div>
            </div>
        </div>
        <div class="gap-80"></div>
        <div class="gap-80"></div>
    </div>
    <div class="section-content section-content-subpage section-services-list animatedParent animateOnce">
        <div class="container">
            <div class="section-caption text-center animated fadeInUp slow">
                <h2 class="mw-750 animated fadeInDownShort">Why Do People Sell Their Life Insurance Policies?</h2>
            </div>
            <div class="gap-80"></div>
            <div class="key-holder life-policies animated fadeInUp slow delay-500">
                <div class="col-md-4 p-0">
                    <div class="white-bg">
                        <p>The insured may have outlived the beneficiaries the policy was originally intended to protect.
                        </p>
                    </div>
                </div>
                <div class="col-md-4 p-0">
                    <div class="white-bg">
                        <p>Premiums may have become unaffordable and unless sold as a life settlement, the insured may have to let the policy lapse.

                        </p>
                    </div>
                </div>
                <div class="col-md-4 p-0">
                    <div class="white-bg">
                        <p>The insured may be financially secure and have no further need for the policy.
                        </p>
                    </div>
                </div>
                <div class="col-md-4 p-0">
                    <div class="white-bg">
                        <p>The insured may wish to make a gift of the monetary value of the policy while they are still alive.
                        </p>
                    </div>
                </div>
                <div class="col-md-4 p-0">
                    <div class="white-bg">
                        <p>The insured may sell the policy for estate planning purposes.
                        </p>
                    </div>
                </div>
                <div class="col-md-4 p-0">
                    <div class="white-bg">
                        <p>The insured is considering whether or not to lapse or surrender the policy, for its cash surrender value.</p>
                    </div>
                </div>
                <div class="col-md-12 p-0">
                    <div class="white-bg">
                        <p>In circumstances where the alternative for the insured is to let the policy lapse and lose a potentially large portion of the premiums which have been paid on the policy, a life settlement is an attractive option.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="gap-80"></div>
    </div>
    <div class="section-content section-about section-content-subpage animatedParent animateOnce">
        <div class="container">
            <div class="section-caption caption-life animated fadeInUp slow delay-250">
                <h3>What is an Expected Rate of Return on a Life Settlement or Viatical?
                </h3>
                <div class="gap-20"></div>
                <p>Life settlements and viaticals as an asset class have provided exceptional returns when held to maturity.  Clients’ portfolios invested in Funds with Exemplify Financial Services, LLC can expect estimated returns on their investment that correspond to the insureds life expectancy (LE). (i.e. 24 month LE = 24% return on investment, 18 month LE = 18%)   Exemplify Financial Services, LLC has no management fees for clients on their IRAs and 401K rollovers.
                </p>
                <div class="gap-50"></div>
            </div>
            <div class="section-caption caption-life animated fadeInUp slow delay-250">
                <h3>Exemplify Holds Funds in a Third-Party Bank Escrow
                </h3>
                <div class="gap-20"></div>
                <p>Life settlement and viatical investments are fractional investments in a trust that holds the legal ownership of the policy and death benefit payout.  Exemplify further protects its clients by having investor funds held in trust by an independent bank escrow account.  Exemplify holds clients’ funds at Nevada Trust Company.  These funds are always kept in an escrow account.  Nevada Trust, as trustee, pays policy premiums through the end of the premium escrow period, and then collects and distributes the death benefit claim from the insurance carrier upon the formal passing of the insured.  The death benefit is distributed by the escrow agent and trustee to the owners of the trust interests on a percentage of ownership basis.
                </p>
                <div class="gap-50"></div>
            </div>
            <div class="section-caption caption-life animated fadeInUp slow delay-250">
                <h3>Who Are Exemplify’s Clients?
                </h3>
                <div class="gap-20"></div>
                <p>Exemplify only accepts clients who are Texas residents, whom wish to invest a minimum of $50,000 in the life settlement and viatical market. Most clients invest either cash or funds rolled over from a traditional IRA, Roth IRA, SEP IRA, self-directed IRA, or a 401K retirement plan.  Exemplify’s clients include accredited investors, as well as trusts and foundations.
                </p>
                <div class="gap-50"></div>
            </div>
        </div>
        <div class="gap-80"></div>
    </div>
    <div class="section-content section-content-subpage section-services-list animatedParent animateOnce">
        <div class="container">
            <div class="section-caption text-center mw-750 animated fadeInUp slow">
                <h2 class="animated fadeInDownShort">The Purchase Process</h2>
                <p>From the initial meeting, should the client choose to move forward, the client receives a ‘welcome aboard’ letter and offering documents, which include:
                </p>
            </div>
            <div class="gap-50"></div>
            <div class="purchase-process animated fadeInUp slow delay-250">
                <ul class="afterclear">
                    <li>
                        Limited Offering Memorandum of the Fund;
                    </li>
                    <li>
                        Client/Purchaser Suitability Questionnaire;
                    </li>
                    <li>
                        Life Settlement/Viatical Purchase Agreement, which outlines the terms of purchase;

                    </li>
                    <li>
                        Subscription Agreement, which describes the policy/asset for purchase; and

                    </li>
                    <li>
                        Escrow Agreement of the Bank as Trustee
                    </li>
                </ul>
                <div class="gap-20"></div>
                <div class="purchase-desc mw-850 text-center">
                    <p>Upon a purchase, the client then receives Exemplify’s welcoming documents, including the confirmation of purchase and specific documentation, and a complete closing package of the details of one or more policy interests purchased and the certificate of purchase issued by the escrow/trustee bank.
                    </p>
                </div>
            </div>
        </div>
        <div class="gap-80"></div>
    </div>
    <div class="section-content section-about section-content-subpage animatedParent animateOnce">
        <div class="container">
            <div class="section-caption caption-life animated fadeInDownShort">
                <div class="diagram-holder">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/diagram.png" alt="">
                </div>
            </div>
            <div class="section-caption caption-life animated fadeInUp slow delay-250">
                <div class="gap-50"></div>
                <h3>Trust Account Maintenance
                </h3>
                <div class="gap-20"></div>
                <p>Once the policy has been purchased, the third-party bank trustee automatically pays the monthly premiums on the policy, and maintains the policy.  Exemplify utilizes a special third-party service to monitor the insured’s life and health.  Exemplify receives quarterly updates on the policies serviced by the Fund, as well as the health and status of the insured’s life.  The insurance carrier provides an annual ‘verification of coverage’ of the policy.</p>

                <p class="animated fadeInUp slow delay-250">Upon the passing of the insured covered by the policy, the trustee bank will file a claim with the life insurance carrier, and the insurance company will pay the claim to the trustee bank holding the trust.  The trustee bank then distributes the funds pro rata to the Exemplify clients according to the fractional share of each in the trust.</p>

                <p class="animated fadeInUp slow delay-250">Once an investor receives their funds from the trust, an investor has an option to reinvest in a new life settlement or Viatical policy.

                </p>
                <div class="gap-50"></div>
            </div>
        </div>
    </div>
    <div class="section-content section-gradient folds bg-inline" style="background-image: url('<?php echo get_template_directory_uri();?>/images/bottom-bg.jpg');">
        <div class="container">
            <div class="white-caption afterclear animatedParent animateOnce">
                <div class="col-md-8 animated fadeInLeft">
                    <h2>Want to Know More?</h2>
                    <p style="margin: 0; max-width: 650px;">
                        Email or call Exemplify Financial Services, LLC today to schedule your free consultation,
                        and maximize your retirement returns today.
                    </p>
                </div>
                <div class="col-md-4 animated fadeInRight">
                    <div class="gap-50"></div>
                    <a href="<?php echo site_url(); ?>/<?php the_field('consultation_link',4); ?>" class="btn-common btn-white">Free Consultation</a>
                </div>
            </div>
        </div>
    </div>
    <?php
endwhile; else :
endif;
get_footer(); ?>