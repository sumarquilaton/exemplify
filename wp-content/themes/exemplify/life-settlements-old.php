<?php
/**
 * Single Life Settlements
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
    ?>
    <div class="section-banner section-gradient banner-subpage">
        <div class="banner-img bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/life-banner.jpg');"></div>
        <div class="caption transform-50 text-center animatedParent animateOnce">
            <h1 class="animated fadeInUpShort"><?php the_title(); ?></h1>
        </div>
    </div>
    <div class="section-content section-content-subpage">
        <div class="container">
            <div class="single-content">
                <div class="animatedParent animateOnce">
                    <div class="col-md-6">
                        <div class="section-caption animated fadeInUp slow">
                            <?php the_field('settlement_description'); ?>
                        </div>
                    </div>
                    <div class="col-md-6 animated fadeInRight slow">
                        <div class="single-service-img-holder">
                            <div class="single-service-img bg-inline" style="background-image: url('<?php the_field('settlement_image'); ?>');"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-gradient folds bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/life-retire.png');">
        <div class="container animatedParent animateOnce">
            <div class="white-caption animated fadeInUp slow afterclear text-center">
                <?php the_field('settlement_blue_content'); ?>
            </div>
        </div>
    </div>
    <div class="section-content pb-6">
        <div class="container">
            <div class="single-content">
                <div class="animatedParent animateOnce">
                    <div class="col-md-6 animated fadeInLeft slow">
                        <div class="single-service-img-holder">
                            <div class="single-service-img bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/life-works.jpg');"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="gap-80"></div>
                        <div class="section-caption pl-4 animated fadeInUp slow">
                            <?php the_field('settlement_works_content'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content section-content-subpage pt-0">
        <div class="container">
            <div class="life-chart animatedParent animateOnce">
                <div class="life-img">
                    <div class="people-top animated fadeInDownShort">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/people-1.png" alt="">
                    </div>
                    <div class="arrow-top animated">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/arrow-1.png" alt="">
                    </div>
                    <div class="life-title animated fadeInUpShort"><h5>You</h5></div>
                </div>

                <div class="life-content animated fadeInDownShort">
                    <div class="btn-common btn-blue">Cash Settlement</div>
                    <div class="btn-common btn-blue">Ongoing & Future Expenses</div>
                </div>

                <div class="life-img">
                    <div class="people-top animated fadeInUpShort">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/people-2.png" alt="">
                    </div>
                    <div class="arrow-top arrow-bottom animated">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/arrow-2.png" alt="">
                    </div>
                    <div class="life-title animated fadeInUpShort"><h5>Investor</h5></div>
                </div>

            </div>
            <div class="single-content">
                <div class="animatedParent animateOnce">
                    <div class="single-iras-content afterclear">
                        <?php $i = 0; while (have_rows('settlement_bottom')): the_row(); ?>
                            <div class="col-md-6 animated <?php if($i%2 == 1) : echo 'fadeInLeftShort'; else : echo 'fadeInRightShort'; endif; ?>">
                                <h3><?php the_sub_field('settlement_bottom_title'); ?></h3>
                                <div class="gap-20"></div>
                                <?php the_sub_field('settlement_bottom_content'); ?>
                            </div>
                            <?php $i++; endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
endwhile; else :
endif;
get_footer(); ?>