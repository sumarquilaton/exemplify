<?php
/**
 * Template Name: News Page
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
<div class="section-banner section-gradient banner-subpage">
    <div class="banner-img bg-inline" style="background-image: url('<?php echo $bg_img[0]; ?>');"></div>
    <div class="caption transform-50 text-center animatedParent animateOnce">
        <h1 class="animated fadeInUpShort">News</h1>
    </div>
</div>
<div class="section-content section-content-subpage animatedParent animateOnce">
    <div class="container">
        <div class="news-grid afterclear">
            <div class="col-md-8 animated fadeInLeft">
                <?php
                query_posts(array(
                        'post_type' => 'post',
                        'posts_per_page' => 10,
                ));
                if ( have_posts() ) : ?>
                    <?php
                    // Start the Loop.
                    while(have_posts()) : the_post(); ?>
                        <a href="<?php the_permalink(); ?>">
                            <div class="news-list">
                                <div class="news-header">
                                    <h3><?php the_title(); ?></h3>
                                    <div class="news-date">
                                        <p><i class="fa fa-calendar" aria-hidden="true"></i> <?php the_time('F j, Y') ?></p>
                                    </div>
                                </div>
                                <div class="news-img-holder">
                                    <div class="news-img bg-inline" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));?>'); "></div>
                                </div>
                                <div class="news-content">
                                    <p>
                                        <?php
                                        $content = get_the_content();
                                        echo mb_strimwidth($content, 0, 500, '...')
                                        ?>
                                    </p>
                                </div>
                                <div class="btn-common btn-blue">Read more</div>
                            </div>
                        </a>
                    <?php endwhile; wp_reset_query(); ?>
                <?php endif; ?>
            </div>
            <div class="col-md-4 animated fadeInRight">
                <div class="news-sidebar">
                    <h3 class="text-uppercase">Recent Post</h3>
                    <div class="sidebar-list">
                        <?php
                        query_posts(array(
                                'post_type' => 'post',
                                'posts_per_page' => 5,
                                'order'        => ASC,
                        ));
                        if ( have_posts() ) : ?>
                            <?php
                            // Start the Loop.
                            while(have_posts()) : the_post(); ?>
                            <div class="sidebar-content">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="news-img-holder">
                                        <div class="news-img bg-inline" style="background-image: url('<?php echo wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));?>'); "></div>
                                    </div>
                                    <div class="news-content">
                                        <p>
                                            <?php
                                            $content = get_the_content();
                                            echo mb_strimwidth($content, 0, 78, '...')
                                            ?>
                                        </p>
                                    </div>
                                </a>
                            </div>
                            <?php endwhile; wp_reset_query(); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
endwhile; else :
endif;
get_footer(); ?>