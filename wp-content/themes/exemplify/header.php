<!DOCTYPE html>
<!--[if IE 7]><html class="no-js ie ie7" lang="en"> <![endif]-->
<!--[if IE 8]><html class="no-js ie ie8" lang="en"> <![endif]-->
<!--[if IE 9]><html class="no-js ie ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en-US" class="no-js">
<!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Exemplify Financial Retirement</title>

	<!-- Bootstrap -->

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

	<?php wp_head(); ?>
</head>
<body class="cbp-spmenu-push">
<div id="page-wrapper">
	<nav class="mobile-nav cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
		<div class="menu-close">
			<button type="button" class="close-menu">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar icon-bar-1"></span>
				<span class="icon-bar icon-bar-2"></span>
				<span class="icon-bar icon-bar-3"></span>
			</button>
		</div>
		<div class="menu-holder">
			<ul class="nav navbar-nav-menu">
				<li class="active menu-item">
					<a href="<?php echo site_url(); ?>">Home</a>
				</li>
				<li class="menu-item">
					<a href="<?php echo site_url(); ?>/services/iras/">IRA's</a>
				</li>
				<li class="menu-item">
					<a href="<?php echo site_url(); ?>/services/life-settlementsviaticals/">Life Settlements</a>
				</li>
				<li class="menu-item">
					<a href="<?php echo site_url(); ?>/services/401k-rollovers/">401K's</a>
				</li>
				<li class="menu-item">
					<a href="<?php echo site_url(); ?>/about-us/">About Us</a>
				</li>
				<li class="menu-item">
					<a href="<?php echo site_url(); ?>/services-page/">Services</a>
				</li>
				<li class="menu-item">
					<a href="<?php echo site_url(); ?>/news/">News</a>
				</li>
				<li class="menu-item">
					<a href="<?php echo site_url(); ?>/contact/">Contact</a>
				</li>
				<li class="menu-item">
					<div class="head-list">
						<div class="head-icon">
							<i class="fa fa-map-marker" aria-hidden="true"></i>
						</div>
						<div class="head-content">
							<?php the_field('address_content', 'option'); ?>
						</div>

					</div>
				</li>
				<li class="menu-item">
					<div class="head-list">
						<div class="head-icon">
							<i class="fa fa-phone"></i>
						</div>
						<div class="head-content">
							<?php the_field('phone_content', 'option'); ?>
						</div>
					</div>
				</li>
				<li class="menu-item">
					<div class="head-list">
						<div class="head-icon">
							<i class="fa fa-globe" aria-hidden="true"></i>
						</div>
						<div class="head-content">
							<?php the_field('web_content', 'option'); ?>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</nav>
	<header class="banner">
		<div class="container p-0">
			<a class="brand" href="<?php echo site_url(); ?>" title="Exemplify">
				<img src="<?php the_field('logo', 'option'); ?>" alt="Exemplify">
			</a>
		</div>
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" id="showRightPush">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="head-contact">
			<div class="container">
				<div class="head-list">
					<div class="head-icon">
						<i class="fa fa-map-marker" aria-hidden="true"></i>
					</div>
					<div class="head-content">
						<?php the_field('address_content', 'option'); ?>
					</div>
				</div>
				<div class="head-list">
					<div class="head-icon">
						<i class="fa fa-phone"></i>
					</div>
					<div class="head-content">
						<?php the_field('phone_content', 'option'); ?>
					</div>
				</div>
				<div class="head-list">
					<div class="head-icon">
						<i class="fa fa-globe" aria-hidden="true"></i>
					</div>
					<div class="head-content">
						<?php the_field('web_content', 'option'); ?>
					</div>
				</div>
			</div>
		</div>
		<div class="container p-0">
			<nav class="nav-primary">
				<div class="menu-main-menu-container">
					<?php
					wp_nav_menu( array(
						'menu'              => 'Main Menu',
						'theme_location'    => 'main-menu',
						'container'         => false,
						'menu_class'        => 'nav')
					);
					?>
				</div>
			</nav>
		</div>
	</header>


