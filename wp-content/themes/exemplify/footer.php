
<div class="section-content footer-section">
	<div class="container p-0">
		<div class="footer-menu afterclear">
			<div class="col-md-3">
				<div class="footer-logo">
					<a href="index.php">
						<img src="<?php the_field('footer_logo', 'option'); ?>" alt="">
					</a>
				</div>
			</div>
			<div class="col-md-6">
				<div class="footer-menu-list">
					<?php
					wp_nav_menu( array(
									'menu'              => 'Footer Menu',
									'theme_location'    => 'footer-menu',
									'container'         => false,
									'menu_class'        => 'footer-menu')
					);
					?>
				</div>
			</div>
			<div class="col-md-3 p-0">
				<div class="footer-consult">
					<a href="<?php echo site_url(); ?><?php the_field('consultation_link', 'option'); ?>" class="btn-common btn-blue">Request a free consultation <i class="fa fa-paper-plane" aria-hidden="true"></i></a>
				</div>
			</div>
		</div>
		<div class="footer-content afterclear">
			<div class="col-md-4">
				<?php the_field('footer_content', 'option'); ?>
			</div>
			<div class="col-md-4">
				<h4>Services</h4>
				<ul>
					<?php
					query_posts(array(
							'post_type' => 'services',
							'posts_per_page' => 10,
					));
					if ( have_posts() ) : ?>
						<?php
						// Start the Loop.
						while(have_posts()) : the_post(); ?>
							<li>
								<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
							</li>
						<?php endwhile; wp_reset_query(); ?>
					<?php endif; ?>
				</ul>
			</div>
			<div class="col-md-4">
				<h4>Contact</h4>
				<div class="footer-contact">
					<div class="footer-contact">
						<div class="icon-social">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-location.png" alt="">
						</div>
						<div class="social-desc">
							<?php the_field('address_content', 'option'); ?>
						</div>
					</div>
					<div class="footer-contact">
						<div class="icon-social">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-phone.png" alt="">
						</div>
						<div class="social-desc">
							<?php the_field('phone_content', 'option'); ?>
						</div>
					</div>
					<div class="footer-contact">
						<div class="icon-social">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icon-site.png" alt="">
						</div>
						<div class="social-desc">
							<?php the_field('web_content', 'option'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="footer-bottom">
	<div class="container p-0">
		<div class="copyright">
			<p><?php the_field('footer_copyright', 'option'); ?></p>
		</div>
		<div class="social-list">
			<ul>
				<li>
					<?php if( get_field('facebook', 'option') ) : ?>
						<a href="<?php the_field('facebook', 'option'); ?>">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
					<?php endif; ?>
				</li>
				<li>
					<?php if( get_field('twitter', 'option') ) : ?>
						<a href="<?php the_field('twitter', 'option'); ?>">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
					<?php endif; ?>
				</li>
				<li>
					<?php if( get_field('youtube', 'option') ) : ?>
						<a href="<?php the_field('youtube', 'option'); ?>">
							<i class="fa fa-youtube" aria-hidden="true"></i>
						</a>
					<?php endif; ?>
				</li>
			</ul>
		</div>
	</div>
</div>
</div>
<?php wp_footer(); ?>
</body>
</html>