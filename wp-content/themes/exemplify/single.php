<?php
/**
 * Single News
 *
 */

get_header();

global $post;
$post_slug=$post->post_name;

$bg_img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full', false, '' );

if ( have_posts() ) : while ( have_posts() ) : the_post();
	?>
	<div class="section-banner section-gradient banner-subpage">
		<div class="banner-img bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/images/banner-contact.jpg');"></div>
		<div class="caption transform-50 text-center animatedParent animateOnce">
			<h1 class="animated fadeInUpShort">News</h1>
		</div>
	</div>
	<div class="section-content section-content-subpage section-sub">
		<div class="container">
			<div class="single-content mw-900">
				<div class="animatedParent animateOnce">
					<div class="single-date animated fadeInDownShort">
						<h3><?php the_time('F j, Y'); ?></h3>
						<div class="sep"></div>
					</div>
					<div class="single-img animated fadeInUpShort bg-inline" style="background-image: url('<?php echo $bg_img[0]; ?>');"></div>
				</div>
				<div class="animatedParent animateOnce">
					<div class="section-caption animated fadeInUp slow">
						<div class="gap-80"></div>
						<div class="sep"></div>
						<h2><?php the_title(); ?></h2>
						<div class="gap-30"></div>
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="section-content section-gradient folds bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/consult-bg.png');">
		<div class="container">
			<div class="white-caption afterclear animatedParent animateOnce">
				<div class="col-md-8 animated fadeInLeft">
					<?php the_field('consultation_content',4); ?>
				</div>
				<div class="col-md-4 animated fadeInRight">
					<a href="<?php echo site_url(); ?>/<?php the_field('consultation_link',4); ?>" class="btn-common btn-white">Free Consultation</a>
				</div>
			</div>
		</div>
	</div>
	<div class="section-content animatedParent animateOnce">
		<div class="container">
			<div class="section-caption text-center">
				<h2 class="animated fadeInUpShort">Recent News</h2>
			</div>
			<div class="news-list afterclear animated fadeInUp slow delay-250">
				<div class="gap-80"></div>
				<?php
				query_posts(array(
						'post_type' => 'post',
						'posts_per_page' => 3,
						'order'     => ASC
				));
				if ( have_posts() ) : ?>
					<?php while(have_posts()) : the_post(); ?>

						<div class="col-md-4">
							<a href="<?php the_permalink(); ?>">
								<div class="news-list-content">
									<h3><?php the_title(); ?></h3>
									<div class="gap-20"></div>
									<p>
										<?php
										$content = get_the_content();
										echo mb_strimwidth($content, 0, 230, '...')
										?>
									</p>
									<div class="gap-30"></div>
									<btn class="btn-common btn-blue">Read more</btn>
								</div>
							</a>
						</div>
					<?php endwhile; wp_reset_query(); ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="section-content section-gradient folds bg-inline" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/images/bottom-bg.jpg');">
		<div class="container animatedParent animateOnce">
			<div class="white-caption afterclear text-center">
				<h2 class="animated fadeInUpShort">Request More Information</h2>
			</div>
			<div class="form-holder text-center afterclear animated fadeInUpShort delay-250">
				<div class="gap-50"></div>
				<?php echo do_shortcode('[contact-form-7 id="105" title="Information"]'); ?>
			</div>
		</div>
	</div>
	<?php
endwhile; else :
endif;
get_footer(); ?>