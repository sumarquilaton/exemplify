<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'exemplify_db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'YL/AP#!7KmtykdpdDOd~Rqlcl<I,7xMLe+gJ<WF|+uVAG_lr%~H}uNf68gM40q_p');
define('SECURE_AUTH_KEY',  'vi1)&W)^G<juhD?j2A#Np!zfv3XM*q6iHZ/AN80Tq0i7.A -b8:RPd3sG,LtP)WH');
define('LOGGED_IN_KEY',    '-D YC}<x1y/^OO;=-^CnUI_1hM-=|j;O S7hKjzt>yACjgeYU|#G3y}P~6A[c4_H');
define('NONCE_KEY',        '3:/hQ20~RiubJ]%b,ZiSHCho4x.7]V[8=>##1O<4QcPZV1QF/n[g{rgYow1X%WZ[');
define('AUTH_SALT',        ')Y,eX15d?!1mI=|BP0l~(`^a#yUM4!UeE~jrzfpNG@YZ)E*rnyh#Q;~0 j6BP;V]');
define('SECURE_AUTH_SALT', '3A~IIVg:V$aZ9}2d{lEDALu.`VeW]1U[7DWjSE@AYW YG`abtB18/}CDpUH5P+(;');
define('LOGGED_IN_SALT',   '3w@hFxnL!Bbeyd[b=>fc 2mP{r`LfQz XDq}|Jrc<EYIO2g7{S>;cPJtD3iwy>jZ');
define('NONCE_SALT',       '53Z.p/%~58FpEm:jdJ.OYzb({$ZXgR/xggj/xtZw{1cfR,vNSR<g:/%JqD/[7DcK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'exe_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
